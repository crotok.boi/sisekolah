package com.si.sekolah.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SekolahController {
	
//	@GetMapping("/test")
	public ResponseEntity<String> test() {
		return new ResponseEntity<>("connected", HttpStatus.OK);
	}
}
