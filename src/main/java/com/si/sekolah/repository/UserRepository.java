package com.si.sekolah.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.si.sekolah.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
